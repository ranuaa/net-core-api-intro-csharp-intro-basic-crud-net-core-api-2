﻿namespace taskDay7.DTOs
{
    public class AddDto
    {
        public string name { get; set; } = string.Empty;
        public List<TaskIputDto> tasks { get; set; } = new List<TaskIputDto>();
    }
}
